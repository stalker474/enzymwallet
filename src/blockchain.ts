var Web3 = require('web3')
import { ACC_TYPE } from './accounts';
import { log } from './utils';
import { LoomProvider, Client, CryptoUtils } from 'loom-js';

//@ts-ignore
const config = require('../artifacts/wallet.json') as Config

interface Web3Subscription {
  on(event : string, callback : any) : void
}

interface Web3Blockheader {

}

export interface IBlockchainListener extends BlockchainListener {
    close() : void
    getHash() : string
    update(chain : ACC_TYPE) : void
}

export class BlockchainListener {
    private lastUpdate : number
    private syncing : boolean
    private freshnessDuration : number
    
    constructor(freshnessDuration : number) {
        this.syncing = false
        this.freshnessDuration = freshnessDuration
        this.lastUpdate = 0
    }
    public beginUpdate() {
        if(this.syncing) {
            return
        }
        this.syncing = true
    }
    public endUpdate() {
        this.syncing = false
        this.lastUpdate = Date.now()
    }
    public isSyncing() : boolean {
        return this.syncing
    }
    public isStale() : boolean {
        return (Date.now() - this.lastUpdate) > this.freshnessDuration
    }
}

export class BlockchainNotifier {
    protected web3Loom : any;
    protected web3Eth : any;
    private loomSubscription : Web3Subscription
    private ethSubscription : Web3Subscription
    private syncers : Map<string, IBlockchainListener>
    private client : Client
    
    private initDone : boolean
    private autoSync : boolean
    
    constructor(autosync : boolean) {
        this.syncers = new Map<string, IBlockchainListener>()
        this.initDone = false
        this.autoSync = autosync
    }
    
    public init() : Promise<void> {
        if(this.initDone) {
            throw "this wallet is already initialized"
        }
        this.initDone = true
        //create default client
        this.client = new Client(
            config.network,
            config.websocket,
            config.query
            )
            this.client.on('error', msg => {
                log("ws connection error :" + msg.error.message)
            })
            
            this.web3Eth = new Web3(new Web3.providers.WebsocketProvider(config.infura))
            this.web3Loom = new Web3(new LoomProvider(this.client, CryptoUtils.generatePrivateKey()))
            
            return new Promise<void>((resolve, reject) => {
                if(!this.autoSync) {
                    resolve()
                    return
                }
                const that = this
                
                let ethOk = false
                //loom doesnt respond often enough to callbacks
                this.loomSubscription = this.web3Loom.eth.subscribe('newBlockHeaders');
                //this one will answer with first block then the callback gets called for each new block so just ignore it
                this.ethSubscription = this.web3Eth.eth.subscribe('newBlockHeaders', function(error : Error, success : Object){
                    if(ethOk) return
                    if(error) {
                        reject("subscribing error :" + error.message)
                    } else {
                        ethOk = true
                        resolve()
                    }
                })
                
                this.loomSubscription.on("data", function(blockHeader : Web3Blockheader){
                    that.syncers.forEach(syncer => { if(!syncer.isSyncing()) syncer.update(ACC_TYPE.LOOM) })
                })
                this.loomSubscription.on("error", (error : any) => {
                    throw "subscriptions failed :" + error 
                });
                
                this.ethSubscription.on("data", function(blockHeader : Web3Blockheader){
                    that.syncers.forEach(syncer => { if(!syncer.isSyncing()) syncer.update(ACC_TYPE.ETHEREUM) })
                })
                this.ethSubscription.on("error", (error : any) => {
                    throw "subscriptions failed :" + error 
                });
            }
            )//end promise
        }
        
        public subscribe(syncer : IBlockchainListener) {
            if(this.syncers.has(syncer.getHash())) {
                throw "listener already exists"
            }
            
            this.syncers.set(syncer.getHash(), syncer)
        }
        
        public getSyncer(hash : string) : IBlockchainListener | undefined {
            return this.syncers.get(hash)
        }
        
        public close() : Promise<void> {
            return new Promise<void>((resolve, reject) => {
                if(!this.initDone)
                {
                    resolve()
                    return
                }

                this.syncers.forEach(syncer => {
                    syncer.close()
                })
                
                if(this.autoSync) {
                    const that = this
                    
                    let incr = 0
                    //@ts-ignore
                    this.loomSubscription.unsubscribe(function(error, success){
                        if(success) {
                            log('Successfully unsubscribed from loom chain!');
                            that.client.disconnect()
                            incr++
                            if(incr > 1) { resolve() }
                        } else {
                            reject(error)
                        }
                    })
                    
                    //@ts-ignore
                    this.ethSubscription.unsubscribe(function(error, success){
                        if(success) {
                            log('Successfully unsubscribed from ethereum chain!');
                            //@ts-ignore
                            that.web3Eth.currentProvider.disconnect()
                            incr++
                            if(incr > 1) { resolve() }
                        } else {
                            reject(error)
                        }
                    })
                } else {
                    this.client.disconnect()
                    //@ts-ignore
                    this.web3Eth.currentProvider.disconnect()
                    resolve()
                }
            })
        }
    }