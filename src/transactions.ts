import { ok } from 'assert';
import { log } from './utils';
import { IBlockchainListener, BlockchainNotifier, BlockchainListener } from './blockchain';
import { ACC_TYPE } from './accounts';

//@ts-ignore
const config = require('../artifacts/wallet.json') as Config

interface Web3TransactionReceipt {

}

export class Receipt {
    public receipt : Web3TransactionReceipt
    
    constructor(receipt : Web3TransactionReceipt) {
        this.receipt = receipt
    }
}

export class Transaction extends BlockchainListener implements IBlockchainListener {
    public hash : string
    public confirmed : number
    public mined : boolean
    public receipt? : Web3TransactionReceipt
    private callbackConf : () => void
    private callbackReceipt : () => void
    private web3 : any
    private chain : ACC_TYPE
    private event : any
    
    constructor(promievent : any, web3 : any, chain : ACC_TYPE, owner : BlockchainNotifier) {
        super(parseInt(config.stale_time))
        this.confirmed = 0
        this.hash = "0x"
        this.mined = false
        this.web3 = web3
        this.chain = chain
        this.event = promievent
        
        promievent.on('transactionHash', (hash : string) => {
            this.hash = hash
            try{
                owner.subscribe(this)
            } catch(error) {
                throw "all transactions have a different hash yet we subscribed a duplicate :" + error
            }
        })
        .once('receipt', (receipt : Web3TransactionReceipt) => {
            this.receipt = receipt
            log('receipt')
            if(this.callbackReceipt) {
                this.callbackReceipt()
            }
        })
        .on('confirmation', (confirmationNumber : number, receipt : Web3TransactionReceipt) => {
            this.confirmed++
            this.receipt = receipt
            log("confirmed " + this.hash)
            if(this.callbackConf && this.confirmed === 1) {
                this.callbackConf()
            }
            if(this.confirmed >= parseInt(config.confirmations_count)) {
                //@ts-ignore
                promievent.removeAllListeners()
                //no longer needing updates from this event
            }
        })
        .on('error', (error : Error) => {
            throw error
        }).then(() => {
            this.mined = true
        })
    }
    
    public getTransactionReceipt(withConfirmation : boolean = false) : Promise<Receipt> {
        return new Promise<Receipt>((resolve) => {
            if(withConfirmation) {
                this.waitConfirmation(() => {
                    ok(this.receipt, "shouldnt be null here")
                    resolve(new Receipt(this.receipt!))
                })
            } else {
                this.waitReceipt(() => {
                    ok(this.receipt, "shouldnt be null here")
                    resolve(new Receipt(this.receipt!))
                })
            }
        })
    }
    
    private waitConfirmation(callback : () => void) {
        this.callbackConf = callback
        if(this.confirmed > 0) {
            this.callbackConf()
        }
    }
    
    private waitReceipt(callback : () => void) {
        this.callbackReceipt = callback
        if(this.receipt) {
            this.callbackReceipt()
        }
    }
    
    public update(chain : ACC_TYPE) : void {
        ok(this.hash !== "0x", "hash cannot be uninitialized here")
        //check that this update is called with the right chain
        if(this.chain !== chain) {
            return
        }
        //only update until first confirmation
        if(this.confirmed >= parseInt(config.confirmations_count)) {
            return
        }
        super.beginUpdate()
        log("Trying to update transaction :" + this.hash)
        this.web3.eth.getTransaction(this.hash).then((data : any) => {
            console.log(data)
            this.endUpdate()
        }).catch((error : any) => {
            log("error getting transaction from web3 :" + error)
            this.endUpdate()
        })
    }
    
    public close() {
        log("closing transaction promievent")
        //@ts-ignore
        this.event.removeAllListeners()
    }

    public getHash() : string {
        ok(this.hash !== "0x", "getHash should never get called on transaction without hash")
        return this.hash
    }
}