var Web3 = require('web3')

import {
    CryptoUtils, Client, LoomProvider, NonceTxMiddleware, SignedTxMiddleware, LocalAddress, Address, OfflineWeb3Signer
} from 'loom-js'
import { BigUInt } from 'loom-js/dist/proto/loom_pb';
import { Config, log } from './utils'
import { AddressMapper } from 'loom-js/dist/contracts/address-mapper';
import { ok } from 'assert';
import { Transaction } from './transactions';
import { BlockchainNotifier, IBlockchainListener, BlockchainListener } from './blockchain';

//@ts-ignore
const config = require('../artifacts/wallet.json') as Config
const erc20JSON = require('../artifacts/erc20abi.json')
const gatewayJSON = require('../artifacts/gateway.json')
type ReadyFunc = () => void

export enum ACC_TYPE {
    ETHEREUM = 1,
    LOOM
}

interface IAccount {
    balance() : Promise<BigUInt>
    balanceZYM() : Promise<BigUInt>
    close() : void
}

class AccountLoom implements IAccount {
    public privateKey : string
    public address : string
    
    private originalPrivateKey : Uint8Array
    
    private client : Client
    private web3 : any
    private owner : BlockchainNotifier
    
    constructor(owner : BlockchainNotifier, seed? : string, loom_privateKey? : string) {
        this.owner = owner
        if(seed) {
            //temporarily instanciate the web3 lib
            this.web3 = new Web3()
            const hash = this.web3.utils.sha3(seed)
            //generate loom
            this.originalPrivateKey = CryptoUtils.generatePrivateKeyFromSeed(CryptoUtils.hexToBytes(hash))
            const publicKey = CryptoUtils.publicKeyFromPrivateKey(this.originalPrivateKey)
            this.privateKey = CryptoUtils.Uint8ArrayToB64(this.originalPrivateKey)
            this.address = LocalAddress.fromPublicKey(publicKey).toChecksumString()
            
            this.client = new Client(config.network,config.websocket,config.query)
            this.client.txMiddleware = [
                new NonceTxMiddleware(publicKey, this.client),
                new SignedTxMiddleware(this.originalPrivateKey)
            ]
            this.client.on('error', msg => {
                throw "Websocket connection error" + msg
            })

            this.web3 = new Web3(new LoomProvider(this.client, this.originalPrivateKey))
        } else if(loom_privateKey) {
            ok((!loom_privateKey!.startsWith("0x")) && (loom_privateKey!.length === 88), "Invalid loom private key :" + loom_privateKey)
            
            this.privateKey = loom_privateKey
            this.originalPrivateKey = CryptoUtils.B64ToUint8Array(loom_privateKey)
            
            const publicKey = CryptoUtils.publicKeyFromPrivateKey(this.originalPrivateKey)
            this.address = LocalAddress.fromPublicKey(publicKey).toChecksumString()
            
            this.client = new Client(config.network,config.websocket,config.query)
            this.client.txMiddleware = [
                new NonceTxMiddleware(publicKey, this.client),
                new SignedTxMiddleware(this.originalPrivateKey)
            ]
            this.client.on('error', msg => {
                throw "Websocket connection error" + msg
            })

            this.web3 = new Web3(new LoomProvider(this.client, this.originalPrivateKey))
        } else {
            throw "invalid arguments, either seedHex or private keys must be set"
        }
    }
    
    public transferZYM(toAddress : string, amount : BigUInt) : Transaction {
        let contract = new this.web3.eth.Contract(erc20JSON.abi, config.ZYM_loom)
        return new Transaction(
            contract.methods.transfer(toAddress, amount.toString()).send({from : this.address}),
            this.web3, 
            ACC_TYPE.LOOM,
            this.owner
            )
        }
        
        public close() {
            this.web3.currentProvider.disconnect()
            this.client.disconnect()
        }
        
        public balance() : Promise<BigUInt> {
            return this.web3.eth.getBalance(this.address)
            .then( (data : any) => {
                let bi = new BigUInt()
                bi.setValue(data.toString())
                return bi
            })
            .catch((error : any) => {
                throw error
            })
        }
        
        public balanceZYM() : Promise<BigUInt> {
            let contract = new this.web3.eth.Contract(erc20JSON.abi, config.ZYM_loom)
            return contract.methods.balanceOf(this.address)
            .call({from : this.address})
            .then((res : any) => {
                let bi = new BigUInt()
                bi.setValue(res)
                return bi
            })
            .catch((error : any) => {
                throw error
            })
        }
        
        public async getMapper() : Promise<AddressMapper | null> {
            const ownerLoomAddr =  Address.fromString(`${this.client.chainId}:${this.address}`)
            const mapperContract = await AddressMapper.createAsync(this.client, ownerLoomAddr)
            try {
                await mapperContract.getMappingAsync(ownerLoomAddr)
                return null
            } catch (err) {
                // assume this means there is no mapping yet, need to fix loom-js not to throw in this case
                return mapperContract
            }
        }
        
        public async mapAccounts(mapper : AddressMapper, signer : OfflineWeb3Signer, eth_address : string) {
            const ownerEthAddr = Address.fromString(`eth:${eth_address}`)
            const ownerLoomAddr =  Address.fromString(`${this.client.chainId}:${this.address}`)
            
            log(`mapping ${ownerEthAddr.toString()} to ${ownerLoomAddr.toString()}`)
            try {
                await mapper.addIdentityMappingAsync(ownerEthAddr, ownerLoomAddr, signer)
                log(`Mapped ${ownerEthAddr} to ${ownerLoomAddr}`)
            } catch(error) {
                throw error
            }
        }
    }
    
    class AccountEthereum implements IAccount {
        
        public privateKey : string
        public address : string
        
        private web3 : any
        private owner : BlockchainNotifier
        
        constructor(owner : BlockchainNotifier, seed? : string, eth_privateKey? : string) {
            this.web3 = new Web3(new Web3.providers.WebsocketProvider(config.infura))
            this.owner = owner
            
            if(seed) {
                const hash = this.web3.utils.sha3(seed)
                //generate only the loom private key from seed
                let loom_originalPrivateKey = CryptoUtils.generatePrivateKeyFromSeed(CryptoUtils.hexToBytes(hash))
                //generate eth by using the same private key as loom but cut it to 64 bytes instead of 128
                this.privateKey = "0x" + CryptoUtils.bytesToHex(loom_originalPrivateKey).substr(0, 64)
                const acc = this.web3.eth.accounts.privateKeyToAccount(this.privateKey)
                this.web3.eth.accounts.wallet.add(acc)
                this.address = acc.address
                
            } else if(eth_privateKey) {
                ok(eth_privateKey!.startsWith("0x") && (eth_privateKey!.length === 66), "Invalid ethereum private key :" + this.privateKey)
                this.privateKey = eth_privateKey
                const acc = this.web3.eth.accounts.privateKeyToAccount(this.privateKey)
                this.web3.eth.accounts.wallet.add(acc)
                this.address = acc.address
            } else {
                throw "invalid arguments, either seedHex or private keys must be set"
            }
        }
        
        public balance() : Promise<BigUInt> {
            return this.web3.eth.getBalance(this.address)
            .then( (data : any) => {
                let bi = new BigUInt()
                bi.setValue(data.toString())
                return bi
            })
            .catch((error : any) => {
                throw error
            })
        }
        
        public balanceZYM() : Promise<BigUInt> {
            const contract = new this.web3.eth.Contract(erc20JSON.abi, config.ZYM_EVM)
            return contract.methods.balanceOf(this.address)
            .call({from : this.address})
            .then((res : any) => {
                let bi = new BigUInt()
                bi.setValue(res)
                return bi
            })
            .catch((error : any) => {
                throw error
            })
        }
        
        public getSigner() : OfflineWeb3Signer {
            const acc = this.web3.eth.accounts.privateKeyToAccount(this.privateKey)
            //incorrect typings make us do that
            //@ts-ignore
            return new OfflineWeb3Signer(this.web3, acc)
        }
        
        async depositTokenToGateway(amount : BigUInt) : Promise<[Transaction, Transaction]> {
            let contractERC20 = new this.web3.eth.Contract(erc20JSON.abi, config.ZYM_EVM)
            let contractGateway = new this.web3.eth.Contract(gatewayJSON.abi, config.GATEWAY)
            
            let gasEstimate = await contractERC20.methods
            .approve(config.GATEWAY, amount.toString())
            .estimateGas({ from: this.address });
            
            let t1 = new Transaction(
                contractERC20.methods.approve(config.GATEWAY, amount.toString())
                .send({ from: this.address, gas: gasEstimate }),
                this.web3, 
                ACC_TYPE.LOOM,
                this.owner
                )
                
                await t1.getTransactionReceipt(true)
                
                gasEstimate = await contractGateway.methods
                .depositERC20(amount.toString(), config.ZYM_EVM)
                .estimateGas({ from: this.address })
                
                let t2 = new Transaction(
                    contractGateway.methods.depositERC20(amount.toString(), config.ZYM_EVM)
                    .send({ from: this.address, gas: gasEstimate }),
                    this.web3, 
                    ACC_TYPE.LOOM,
                    this.owner
                    )
                    
                    return [t1, t2]
                }
                
                public close() {
                    this.web3.currentProvider.disconnect()
                }
            }
            
            export class ZymAccount extends BlockchainListener implements IBlockchainListener {
                
                public mapped : boolean
                public ethBalance : BigUInt
                public ethZymBalance : BigUInt
                public loomBalance : BigUInt
                public loomZymBalance : BigUInt
                
                private ethAcc : AccountEthereum
                private loomAcc : AccountLoom
                private onReadyFunc : ReadyFunc
                
                constructor(readyFunc : ReadyFunc, owner : BlockchainNotifier, seed? : string, loom_privateKey? : string, eth_privateKey? : string) {
                    super(parseInt(config.stale_time))
                    this.ethAcc = new AccountEthereum(owner, seed, eth_privateKey)
                    this.loomAcc = new AccountLoom(owner, seed, loom_privateKey)
                    this.onReadyFunc = readyFunc
                    
                    this.loomAcc.getMapper().then(mapper => {
                        //the promise will fill mapper with non null if not mapped yet
                        if(mapper) {
                            this.loomAcc.mapAccounts(mapper, this.ethAcc.getSigner(), this.ethAcc.address).then(d => {
                                //loom and eth accounts are now mapped
                                this.onReadyFunc()
                            })
                        } else {
                            //loom and eth accounts were already mapped
                            this.onReadyFunc()
                        }
                    }).catch(error => {
                        throw error
                    })
                }
                
                public update(chain : ACC_TYPE) {
                    super.beginUpdate()
                    ok(!super.isSyncing(), "Shouldnt be called if already syncing")
                    
                    let p : Promise<BigUInt>[] = []
                    
                    if(chain == ACC_TYPE.ETHEREUM) {
                        p.push(this.ethAcc.balance())
                        p.push(this.ethAcc.balanceZYM())
                    } else if(chain == ACC_TYPE.LOOM){
                        p.push(this.loomAcc.balance())
                        p.push(this.loomAcc.balanceZYM())
                    }
                    
                    Promise.all(p).then(balances => {
                        if(chain == ACC_TYPE.ETHEREUM) {
                            this.ethBalance = balances[0]
                            this.ethZymBalance = balances[1]
                        } else if(chain == ACC_TYPE.LOOM){
                            this.loomBalance = balances[0]
                            this.loomZymBalance = balances[1]
                        }
                        super.endUpdate()
                        log("syncing done on :" + this.getEthAddress() + " for chain :" + chain)
                    }).catch(error => {
                        log("syncing error :" + error)
                        super.endUpdate()
                    })
                }
                
                public close() {
                    this.ethAcc.close();
                    this.loomAcc.close();
                }
                
                public balance(account : ACC_TYPE) : Promise<BigUInt> {
                    //dont call the balance fetcher if data isnt stale
                    if(!super.isStale()) {
                        return new Promise<BigUInt>((resolve, reject) => {
                            if(account == ACC_TYPE.ETHEREUM) {
                                resolve(this.ethBalance)
                            } else {
                                resolve(this.loomBalance)
                            }
                        })   
                    }
                    let acc : IAccount = account === ACC_TYPE.LOOM? this.loomAcc : this.ethAcc
                    return acc.balance()
                }
                
                public balanceZYM(account : ACC_TYPE) : Promise<BigUInt> {
                    //dont call the balance fetcher if data isnt stale
                    if(!super.isStale()) {
                        return new Promise<BigUInt>((resolve, reject) => {
                            if(account == ACC_TYPE.ETHEREUM) {
                                resolve(this.ethZymBalance)
                            } else {
                                resolve(this.loomZymBalance)
                            }
                        })   
                    }
                    let acc : IAccount = account === ACC_TYPE.LOOM? this.loomAcc : this.ethAcc
                    return acc.balanceZYM()
                }
                
                public transferZYM(toAddress : string, amount : BigUInt) : Transaction {
                    return this.loomAcc.transferZYM(toAddress, amount)
                }
                
                public transferZYMToEthereum(amount : BigUInt) : Promise<[Transaction, Transaction]> {
                    return this.ethAcc.depositTokenToGateway(amount)
                }
                
                public getLoomAddress() : string {
                    return this.loomAcc.address
                }
                
                public getEthAddress() : string {
                    return this.ethAcc.address
                }
                
                public getHash() : string {
                    return this.getLoomAddress()
                }
            }