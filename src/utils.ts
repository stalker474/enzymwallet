export interface Config {
    network : string,
    websocket : string,
    query : string,
    api_url : string,
    ZYM_loom : string,
    ZYM_EVM : string,
    infura : string,
    GATEWAY : string,
    stale_time : string,
    confirmations_count : string
}

export var debug = true;

export function log(message : string) {
  if (debug) { 
    console.log(message);
  }
}