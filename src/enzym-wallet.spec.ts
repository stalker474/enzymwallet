
import * as EnzymWallet from './enzym-wallet'
import {ACC_TYPE} from './accounts'

// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
import 'mocha';
import { assert } from 'chai';
import { BigUInt } from 'loom-js/dist/proto/loom_pb';

const privateKeyLoom = "m1krBot9cIgHeZDSQkH5kimOpbT6b/GoEN9IPBHuR8DkBhSpOMHmiVvw9+yh4/W7nRmobKmxvG5bEHtUvscaiQ=="
const privateKeyEth = "0xf251eae7d472a1fe2ff6000433f03dedc849097f47c2585600cfa7fc18afd327"


describe('Enzym Wallet', () => {
  let wallet : EnzymWallet.Wallet
  let mainAccount : EnzymWallet.ZymAccount
  
  before(async function() {
    wallet = new EnzymWallet.Wallet(false)
    
    try {
      console.log("creating wallet")
      await wallet.init()

      let acc = await wallet.importAccountFromKeys(privateKeyLoom, privateKeyEth)
      assert.isNotNull(acc)
      mainAccount = acc!
    } catch(error) {
      console.log(error)
    }
    
    console.log("==============================")
    console.log("==============================")
    console.log("==============================")
    console.log("==============================")
    console.log("==============================")
    console.log("==============================")
  });
  
  after(async function() {
    try {
      console.log("closing wallet")
      await wallet.close()
      console.log("wallet closed")
    } catch(error) {
      console.log(error)
    }
  });

  /*it('should always be able to close both sync and async wallet listeners', async () => {
    let w1 = new EnzymWallet.Wallet(false)
    let w2 = new EnzymWallet.Wallet(true)
    await w1.init()
    await w2.init()
    await w1.close()
    await w2.close()
  });
  
  it('should always generate the same pair of addresses from the same seed', async () => {
    let acc = await wallet.importAccountFromSeed("enzym")
    let w2 = new EnzymWallet.Wallet(false)
    await w2.init()
    let acc2 = await w2.importAccountFromSeed("enzym")
    assert.isNotNull(acc)
    assert.isNotNull(acc2)
    assert.equal(acc!.getLoomAddress(), acc2!.getLoomAddress())
    assert.equal(acc!.getEthAddress(), acc2!.getEthAddress())
    await w2.close()
  });

  it('should be able to fetch ZYM and ETH balance on loom', async () => {
    const balance1 = await mainAccount.balance(ACC_TYPE.LOOM)
    const balance2 = await mainAccount.balanceZYM(ACC_TYPE.LOOM)
    console.log(balance1.toString(), balance2.toString())
  });
  
  it('should be able to fetch ZYM and ETH balance on EVM', async () => {
    const balance1 = await mainAccount.balance(ACC_TYPE.ETHEREUM)
    const balance2 = await mainAccount.balanceZYM(ACC_TYPE.ETHEREUM)
    console.log(balance1.toString(), balance2.toString())
  });*/
  
  it('should be able to generate a transaction to transfer zym to another address', async () => {
    let acc2 = await wallet.newAccount()
    assert.isNotNull(acc2)
    console.log("sending from " + mainAccount.getLoomAddress() + " to " + acc2!.getLoomAddress() + " ...")
    let amount = new BigUInt()
    amount.setValue('1')
    let trans = mainAccount.transferZYM(acc2!.getLoomAddress(), amount);
    let receipt = await trans.getTransactionReceipt()
    console.log(receipt.receipt)
    await trans.getTransactionReceipt(true)
  });
  /*
  it('should be able to generate a transaction to transfer zym from loom to ethereum', async () => {
    let amount = new BigUInt()
    amount.setValue('1')
    let transactions = await mainAccount.transferZYMToEthereum(amount)
    //first transaction is always confirmed
    let receipt1 = await transactions[0].getTransactionReceipt(false)
    let receipt2 = await transactions[1].getTransactionReceipt(false)
    
    let confirmation1 = await transactions[0].getTransactionReceipt(true)
    let confirmation2 = await transactions[1].getTransactionReceipt(true)
    
    console.log(receipt1, receipt2, confirmation1, confirmation2)
  });*/
});
