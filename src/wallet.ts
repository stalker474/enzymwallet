var Web3 = require('web3')

import { ZymAccount } from './accounts';
import { BlockchainNotifier } from './blockchain';

export class Wallet extends BlockchainNotifier {
    private accounts : ZymAccount[]
    
    constructor(autosync : boolean) {
        super(autosync)
        this.accounts = []
    }

    public close() : Promise<void> {
        return super.close()
    }

    public findAccount(loomAddress : string) : ZymAccount {
        let elem = this.getSyncer(loomAddress)
        if(elem === undefined)
         throw "account not found : " + loomAddress
        if(! (elem instanceof ZymAccount))
         throw "did you provide a transaction hash instead of account address?"
        return elem as ZymAccount
    }

    public newAccount() : Promise<ZymAccount | null> {
        //generate random seed
        const seed = new Web3().eth.accounts.create().privateKey
        return this.importAccountFromSeed(seed)
    }

    public importAccountFromSeed(seed : string) : Promise<ZymAccount | null> {
        return new Promise<ZymAccount>((resolve, reject) => {
            //import an account through its seed
            try{
                let ready = () => {
                    this.addAccount(acc)
                    resolve(acc)
                }
                let acc = new ZymAccount(ready, this, new Web3().utils.sha3(seed))
            } catch(error) {
                reject(error)
            }
        })
    }

    public importAccountFromKeys(loom_privateKey : string, eth_privateKey : string) : Promise<ZymAccount | null> {
        return new Promise<ZymAccount>((resolve, reject) => {
            //import an account through its seed
            try{
                let ready = () => {
                    this.addAccount(acc)
                    resolve(acc)
                }
                let acc = new ZymAccount(ready, this, undefined, loom_privateKey, eth_privateKey)
            } catch(error) {
                reject(error)
            }
        })
    }

    protected addAccount(acc : ZymAccount) {
        try{
            this.subscribe(acc)
            this.accounts.push(acc)
        } catch(error) {
            throw "account already exists"
        }
    }
  }