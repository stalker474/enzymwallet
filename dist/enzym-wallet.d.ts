import { Client } from 'loom-js';
import { BigUInt } from 'loom-js/dist/proto/loom_pb';
export declare class Address {
    publicKey: Uint8Array;
    privateKey?: Uint8Array;
    constructor(currClient?: Client, privateKey?: Uint8Array, publicKey?: Uint8Array);
}
export declare class Wallet {
    private addresses;
    private client?;
    private web3?;
    private httpApi?;
    private provider?;
    constructor(isHttp: boolean);
    close(): void;
    newAddress(): string;
    importAddressFromPrivateKey(privateKey: string): string;
    balance(publicKey: string): Promise<BigUInt>;
    balanceZYM(publicKey: string): Promise<BigUInt>;
    private balance_web3;
    private balance_url;
    private balanceZYM_web3;
    private balanceZYM_url;
}
