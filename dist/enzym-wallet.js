"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const web3_1 = __importDefault(require("web3"));
const loom_js_1 = require("loom-js");
const loom_pb_1 = require("loom-js/dist/proto/loom_pb");
// import { AbstractMethod } from 'web3-core-method';
// import { AbstractWeb3Module } from 'web3-core';
const assert_1 = require("assert");
const node_fetch_1 = __importDefault(require("node-fetch"));
//@ts-ignore
const config = require('../artifacts/wallet.json');
const erc20 = require('../artifacts/erc20abi.json');
class Address {
    constructor(currClient, privateKey, publicKey) {
        assert_1.ok(publicKey || privateKey, "Can't construct address without either key");
        if (!privateKey) {
            this.publicKey = publicKey;
        }
        else {
            this.publicKey = loom_js_1.CryptoUtils.publicKeyFromPrivateKey(privateKey);
            this.privateKey = privateKey;
        }
    }
}
exports.Address = Address;
class Wallet {
    constructor(isHttp) {
        if (isHttp) {
            this.httpApi = config.api_url;
        }
        else {
            this.client = new loom_js_1.Client(config.network, config.websocket, config.query);
            this.provider = new loom_js_1.LoomProvider(this.client, loom_js_1.CryptoUtils.generatePrivateKey());
            //typescript refuses to accept LoomProvider as argument but looks like its wrong here
            //@ts-ignore
            this.web3 = new web3_1.default(this.provider);
        }
        this.addresses = [];
    }
    close() {
        if (this.client) {
            console.log("closing");
            //this.provider!.disconnect()
            //this.client.disconnect()
        }
    }
    newAddress() {
        const privateKeyHex = loom_js_1.CryptoUtils.generatePrivateKey().toString();
        return this.importAddressFromPrivateKey(privateKeyHex).toString();
    }
    importAddressFromPrivateKey(privateKey) {
        const key = loom_js_1.CryptoUtils.B64ToUint8Array(privateKey);
        this.addresses.push(new Address(this.client, key));
        return this.addresses[this.addresses.length - 1].publicKey.toString();
    }
    balance(publicKey) {
        assert_1.ok(this.web3 || this.httpApi, "can't fetch balance without any api");
        if (this.web3) {
            return this.balance_web3(publicKey);
        }
        else {
            return this.balance_url(publicKey);
        }
    }
    balanceZYM(publicKey) {
        assert_1.ok(this.web3 || this.httpApi, "can't fetch balance without any api");
        if (this.web3) {
            return this.balanceZYM_web3(publicKey);
        }
        else {
            return this.balanceZYM_url(publicKey);
        }
    }
    balance_web3(publicKey) {
        return __awaiter(this, void 0, void 0, function* () {
            assert_1.ok(this.web3, "no web3");
            return new Promise((resolve, reject) => {
                this.web3.eth.getBalance(publicKey).then(data => {
                    let bi = new loom_pb_1.BigUInt();
                    bi.setValue(data);
                    resolve(bi);
                }, error => {
                    reject(error);
                });
            });
        });
    }
    balance_url(publicKey) {
        return __awaiter(this, void 0, void 0, function* () {
            assert_1.ok(this.httpApi, "no http api");
            const query = '?module=account&action=balance&address=' + publicKey;
            return new Promise((resolve, reject) => {
                node_fetch_1.default(this.httpApi + query)
                    .then(res => res.json())
                    .then(res => {
                    let bi = new loom_pb_1.BigUInt();
                    bi.setValue(res.result);
                    resolve(bi);
                })
                    .catch(error => {
                    reject(error);
                });
            });
        });
    }
    balanceZYM_web3(publicKey) {
        return __awaiter(this, void 0, void 0, function* () {
            assert_1.ok(this.web3, "no web3");
            return new Promise((resolve, reject) => {
                let contract = new this.web3.eth.Contract(erc20.abi, config.ZYM_contact_addr);
                contract.methods.balanceOf(publicKey).call({ from: '0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe' }).then((data) => {
                    let bi = new loom_pb_1.BigUInt();
                    bi.setValue(data);
                    resolve(bi);
                }).catch((error) => {
                    reject(error);
                });
            });
        });
    }
    balanceZYM_url(publicKey) {
        return __awaiter(this, void 0, void 0, function* () {
            assert_1.ok(this.httpApi, "no http api");
            const query = '?module=account&action=tokenbalance&contractaddress=' + config.ZYM_contact_addr + '&address=' + publicKey;
            return new Promise((resolve, reject) => {
                node_fetch_1.default(this.httpApi + query)
                    .then(res => res.json())
                    .then(res => {
                    let bi = new loom_pb_1.BigUInt();
                    bi.setValue(res.result);
                    resolve(bi);
                })
                    .catch(error => {
                    reject(error);
                });
            });
        });
    }
}
exports.Wallet = Wallet;
//# sourceMappingURL=enzym-wallet.js.map